#ifdef __CINT__

#include "BJetMatrixMethod/MatrixMethod.h"

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class BJetMatrixMethod::MatrixMethod+;

#endif
