/**
 * @file MatrixMethod.cxx
 * @author Antoine Marzin <antoine.marzin@cern.ch>
 * @date November, 2012
 * @brief Estimate the number of background events with at least 1 fake b-jet in a 3 b-jets selection via a matrix method. 
 **/ 

#include "BJetMatrixMethod/MatrixMethod.h"

#include <algorithm>


namespace BJetMatrixMethod {
  float MatrixMethod::m_ptbins[6] = {20000,60000,90000,120000,150000,200000}; 
  float MatrixMethod::m_etabins[3] = {0.,1.2,2.5};


  MatrixMethod::MatrixMethod() 
  {  
  }


  void MatrixMethod::initialize(const std::string &filename)
  {
    m_file = new TFile(filename.c_str());

    h_eff_fake_70 = (TH2F*)m_file->Get("eff_fake_70");
    h_eff_fake_70->SetDirectory(0);
    h_eff_fake_stat_70 = (TH2F*)m_file->Get("eff_fake_stat_70");
    h_eff_fake_stat_70->SetDirectory(0);
    h_eff_fake_syst_70 = (TH2F*)m_file->Get("eff_fake_syst_70"); 
    h_eff_fake_syst_70->SetDirectory(0);
  }

  std::pair<float, float > MatrixMethod::GetFakeEfficiency(float eta, float pt, std::string OP) 
  {
    double eff_fake, eff_fake_stat, eff_fake_syst;	

    // search the corresponding eta bin
    eta = fabs(eta);
    int eta_bin = -1;
    for (unsigned int i = 0; i < 2; ++i) {
      if( (eta >= m_etabins[i]) && (eta < m_etabins[i+1]) ) {
        eta_bin = i+1;
        break;
      }
    }
    if (eta_bin == -1) return std::pair<float, float>(0.f, 0.f);

    /// search the corresponding pt bin
    int pt_bin = -1;		
    for(unsigned int i = 0; i < 5; ++i) {
      if( (pt >= m_ptbins[i]) && (pt < m_ptbins[i+1]) ) {
        pt_bin = i+1;
        break; 
      } else if( pt >= m_ptbins[5]) {
        pt_bin = 6;
        break;
      }
    }
    if (pt_bin == -1) return std::pair<float, float>(0.f, 0.f);

    if (OP.compare("0_9827") == 0) { // 60% 
      std::cerr << "60\% operating point unsupported!" << std::endl;
      throw;
    } else if (OP.compare("0_7892") == 0) { // 70% 
      eff_fake = h_eff_fake_70->GetBinContent(eta_bin,pt_bin);
      eff_fake_stat = h_eff_fake_stat_70->GetBinContent(eta_bin,pt_bin);
      eff_fake_syst = h_eff_fake_syst_70->GetBinContent(eta_bin,pt_bin);		  
    } else if (OP.compare("0_3511") == 0) { /// 80% 
      std::cerr << "80\% operating point unsupported!" << std::endl;
      throw;
    } else {
      std::cerr << "Unknown operating point: " << OP << std::endl;
      throw;
    }

    float eff_fake_err = sqrt(eff_fake_stat*eff_fake_stat + eff_fake_syst*eff_fake_syst);

    std::pair<float, float> efficiency (eff_fake, eff_fake_err);

    return efficiency;
  }

  std::pair<float, float > MatrixMethod::GetRealEfficiency(float eta, float pt, float MV1, 
      std::string taggerName, std::string OP, 
      std::string calibration, std::string calibfolder)   
  {
    /// Create the BTagCalib object
    static Analysis::CalibrationDataInterfaceROOT* BTagCalib = new Analysis::CalibrationDataInterfaceROOT(taggerName,calibration,calibfolder);

    /// Set BTag Calibration variables
    std::string label = "B";      

    Analysis::CalibrationDataVariables BTagVars;
    BTagVars.jetAuthor = "AntiKt4TopoLCnoJVF";
    BTagVars.jetPt  = pt;
    BTagVars.jetEta = eta;

    /// Set the b-tagging uncertainty
    Analysis::Uncertainty BTagUnc = Analysis::Total; /// None, Total, Statistical, Systematic

    /// Get the b-tagging efficiencies
    std::pair<double, double> BTagCalibEff = BTagCalib->getEfficiency(BTagVars, label, OP, BTagUnc);

    return BTagCalibEff;	
  }


  // Return <weight, variance>
  std::pair<float, float> MatrixMethod::getWeight(const std::vector<float> &v_pt, const std::vector<float>
      &v_eta, const std::vector<float> &v_MV1, const std::string &taggerName,
      const std::string &OP, float opval, const std::string &calibration,
      const std::string &calibfolder)
  {
    unsigned int nJets = v_pt.size();

    // No point continuing with fewer than 3 jets!
    if (nJets < 3) {
      return std::make_pair(0.f, 0.f);
    }

    // FIXME
    // Truncate the problem to save runtime
    if (nJets > 12) nJets = 12;

    // Vector of real/fake b-tagging efficiency for all selected jets, and
    // uncertainties
    float *effReal = new float [nJets];
    float *effFake = new float [nJets];
    float *uEffReal = new float [nJets];
    float *uEffFake = new float [nJets];

    // The TL_in configuration of the event
    char *configTLIn = new char [nJets];

    // Fill initial information
    for (unsigned int i = 0; i < nJets; ++i) { 
      float eta = v_eta[i];
      float pt = v_pt[i];
      float MV1 = v_MV1[i];

      const std::pair<float, float> &realEfficiency = this->GetRealEfficiency(eta, pt, MV1, taggerName, OP, calibration, calibfolder);
      const std::pair<float, float> &fakeEfficiency = this->GetFakeEfficiency(eta, pt, OP);

      effReal[i] = realEfficiency.first;
      uEffReal[i] = realEfficiency.second;
      effFake[i] = fakeEfficiency.first;
      uEffFake[i] = fakeEfficiency.second;

      if (MV1 > opval) configTLIn[i] = 1;
      else configTLIn[i] = 0;
    }

    // Configurations used for iteration
    char *configTLOut = new char [nJets];
    char *configRF = new char [nJets];
    std::fill(configTLOut, configTLOut+nJets, 0);
    std::fill(configRF, configRF+nJets, 0);
    
    float weight = 0.;
    float variance = 0.;
    while (!isLastConfiguration(configTLOut, nJets)) {
      if (!isWantedTLOutConfiguration(configTLOut, nJets)) {
        incrementConfiguration(configTLOut, nJets);
        continue;
      }

      while (!isLastConfiguration(configRF, nJets)) {
        if (!isWantedRFConfiguration(configRF, nJets)) {
          incrementConfiguration(configRF, nJets);
          continue;
        }

        float matrixElement = getMatrixElement(configTLOut, configRF, effReal, effFake, nJets);
        float inverseFactor = getInverseFactor(configRF, configTLIn, effReal, effFake, nJets);

        weight += matrixElement * inverseFactor;
        variance += getEfficiencyVariance(configTLOut, configRF, configTLIn, matrixElement, inverseFactor, effReal, effFake, uEffReal, uEffFake, nJets);

        //std::cout << isWantedTLOutConfiguration(configTLOut, nJets) << " " << isWantedRFConfiguration(configRF, nJets) << "; " << matrixElement << " " << inverseFactor << std::endl;
        //for (unsigned int j = 0; j < nJets; ++j) {
        //  std::cout << configTLOut[j];
        //}
        //std::cout << std::endl;
        
        incrementConfiguration(configRF, nJets);
      }
      
      std::fill(configRF, configRF+nJets, 0);
      incrementConfiguration(configTLOut, nJets);
    }

    // Clean up
    delete [] effReal;
    delete [] effFake;
    delete [] uEffReal;
    delete [] uEffFake;
    delete [] configTLIn;
    delete [] configTLOut;
    delete [] configRF;
    
    // We DO NOT WANT the Statistical component of variance!!
    //variance += pow(weight, 2);

    return std::make_pair(weight, variance);
  }


  bool MatrixMethod::isLastConfiguration(const char *config, unsigned int length)
  {
    for (unsigned int i = 0; i < length; ++i ) {
      if (config[i] > 1) return true;
    }
    return false;
  }

  void MatrixMethod::incrementConfiguration(char *config, unsigned int length)
  {
    for (unsigned int i = 0; i < length; ++i ) {
      if (config[i] == 0) {
        config[i] = 1;
        return;
      } else {
        config[i] = 0;
      }
    }
    
    // Flag as finished
    if (length > 0) config[0] = 2;
  }

  bool MatrixMethod::isWantedTLOutConfiguration(const char *config, unsigned int length)
  {
    // We want at least 3 tight...
    unsigned int nTight = 0;
    for (unsigned int i = 0; i < length; ++i ) {
      if (config[i] == 1) ++nTight;
    }
    return (nTight > 2);
  }

  bool MatrixMethod::isWantedRFConfiguration(const char *config, unsigned int length)
  {
    // We want fewer than 3 real b jets
    unsigned int nReal = 0;
    for (unsigned int i = 0; i < length; ++i ) {
      if (config[i] == 1) ++nReal;
    }
    return (nReal < 3);
  }

  float MatrixMethod::getMatrixElement(const char *configTLOut, const char *configRF, const float *effReal,
      const float *effFake, unsigned int length)
  {
    float element = 1.f;
    for (unsigned int i = 0; i < length; ++i ) {
      if (configTLOut[i] == 1 && configRF[i] == 1) {
        element *= effReal[i];
      }
      else if (configTLOut[i] == 1 && configRF[i] == 0) {
        element *= effFake[i];
      }
      else if (configTLOut[i] == 0 && configRF[i] == 1) {
        element *= 1.f - effReal[i];
      }
      else if (configTLOut[i] == 0 && configRF[i] == 0) {
        element *= 1.f - effFake[i];
      }
    }
    return element;
  }


  float MatrixMethod::getInverseFactor(const char *configRF, const char *configTLIn, const float *effReal,
      const float *effFake, unsigned int length)
  {
    float element = 1.f;
    for (unsigned int i = 0; i < length; ++i ) {
      // Pick out cofactor
      if (configTLIn[i] == 1 && configRF[i] == 1) {
        element *= 1.f - effFake[i];
      }
      else if (configTLIn[i] == 0 && configRF[i] == 1) {
        element *= - effFake[i];
      }
      else if (configTLIn[i] == 1 && configRF[i] == 0) {
        element *= - (1.f - effReal[i]);
      }
      else if (configTLIn[i] == 0 && configRF[i] == 0) {
        element *= effReal[i];
      }
      
      // Divide by determinant
      element /= effReal[i] - effFake[i];
    }
    return element;
  }

  float MatrixMethod::getEfficiencyVariance(const char *configTLOut, const char *configRF, const char *configTLIn, float matrixElement, float inverseFactor, const float *effReal, const float *effFake, const float *uEffReal, const float *uEffFake, unsigned int length)
  {
    float variance = 0.f;
    for (unsigned int i = 0; i < length; ++i ) {
      float de_M = 0.f, de_iF = 0.f, df_M = 0.f, df_iF = 0.f;

      // de_M
      if (configRF[i] == 1 && configTLOut[i] == 1) {
        if (effReal[i] > 0.f) de_M = matrixElement / effReal[i];
      } else if (configRF[i] == 1 && configTLOut[i] == 0) {
        if (effReal[i] < 1.f) de_M = - matrixElement / (1.f - effReal[i]);
      }
      
      // df_M
      if (configRF[i] == 0 && configTLOut[i] == 1) {
        if (effFake[i] > 0.f) df_M = matrixElement / effFake[i];
      } else if (configRF[i] == 0 && configTLOut[i] == 0) {
        if (effFake[i] < 1.f) df_M =  - matrixElement / (1.f - effFake[i]);
      }

      // de_iF
      if (configRF[i] == 0 && configTLIn[i] == 0) {
        if (effReal[i] > 0.f) de_iF = inverseFactor / effReal[i];
      } else if (configRF[i] == 0 && configTLIn[i] == 1) {
        if (effReal[i] < 1.f) de_iF = inverseFactor / (1.f - effReal[i]);
      }
      
      // df_iF
      if (configRF[i] == 1 && configTLIn[i] == 0) {
        if (effFake[i] > 0.f) df_iF = - inverseFactor / effFake[i];
      } else if (configRF[i] == 1 && configTLIn[i] == 1) {
        if (effFake[i] < 1.f) df_iF = - inverseFactor / (1.f - effFake[i]);
      }
      //std::cout << matrixElement << " " << inverseFactor << ";  " << effReal[i] << " " << effFake[i] << ";  " << de_M << " " << de_iF << " " << df_M << " " << df_iF << std::endl;

      float diffReal = de_M * inverseFactor + matrixElement * de_iF - matrixElement * inverseFactor / (effReal[i] - effFake[i]);
      float diffFake = df_M * inverseFactor + matrixElement * df_iF + matrixElement * inverseFactor / (effReal[i] - effFake[i]);

      variance += pow(diffReal * uEffReal[i], 2);
      variance += pow(diffFake * uEffFake[i], 2);
    }
    return variance;
  }
}

