/**
 * @file MatrixMethod.h
 * @author Antoine Marzin <antoine.marzin@cern.ch>
 * @date November, 2012
 * @brief Estimate the number of background events with at least 1 fake b-jet in a 3 b-jets selection via a matrix method. 
 *
 * Return an event weight per event
 * Example usage:
 * step 1 : declare and initialize the matrix method with the root file containing the fake rates : 
 *  MatrixMethod* myMM = new MatrixMethod();
 *  myMM->initialize("MatrixMethod/data/MM_fake_rate_b.root");
 * step 2 : loop over events from data before any b-tagging requirement and retreive the event weight corresponding to the chosen operating point. 
 *  you need the following arguments : 
 *   - three vectors containing the pt, eta and MV1 weight values for all jets which satisfy the pt and eta cuts applied to b-jets 
 *     (for instance if you require >= 3 b-jets with pt > 50 GeV and |eta|<2.5, consider only jets with pt > 50 GeV and |eta|<2.5)  
 *     - v_pt : vector of pt  (in MeV)
 *     - v_eta : vector of eta 
 *     - v_MV1 : vector of MV1 weight
 *   - The name of the b-tagging algorithm : ex MV1 
 *   - A string containing the cut value of the b-tagging operating point : ex "0_595" for the 75% operating point 
 *   - A float containing the  cut value of the b-tagging operating point : ex 0.595 for the 75% operating point 
 *   - The path to the BTagCalibration.env file 
 *   - The path to the directory where is stored the b-tagging calibration file
 *  Exemple :
 *   double weight_75 = myMM->GetWeight(v_pt,v_eta,v_MV1,"MV1","0_595",0.595,"SUSYTools/data/BTagCalibration.env","SUSYTools/data/");  
 *   double weight_70 = myMM->GetWeight(v_pt,v_eta,v_MV1,"MV1","0_772",0.772,"SUSYTools/data/BTagCalibration.env","SUSYTools/data/");  
 *   double weight_60 = myMM->GetWeight(v_pt,v_eta,v_MV1,"MV1","0_980",0.98,"SUSYTools/data/BTagCalibration.env","SUSYTools/data/");  
 * step 3 : sum all the event weights to get the background estimate
 **/ 

#ifndef MyPackage_MATRIXMETHOD_BJETS_H
#define MyPackage_MATRIXMETHOD_BJETS_H

#include "TObject.h"
#include "TH2F.h"
#include "TFile.h"
#include <TMath.h>
#include <iostream>
#include <utility>
#include <TMatrixD.h>
#include "CalibrationDataInterface/CalibrationDataInterfaceROOT.h"



namespace BJetMatrixMethod {
  class MatrixMethod 
  {
  public:
    MatrixMethod();
    ~MatrixMethod() {};
    virtual void initialize(const std::string &filename);

    std::pair<float, float > GetFakeEfficiency(float eta, float pt, std::string OP);
    std::pair<float, float > GetRealEfficiency(float eta, float pt, float MV1, std::string taggerName, 
                 std::string OP, std::string calibration, std::string calibfolder);

    std::pair<float, float> getWeight(const std::vector<float> &v_pt, const std::vector<float>
        &v_eta, const std::vector<float> &v_MV1, const std::string &taggerName,
        const std::string &OP, float opval, const std::string &calibration,
        const std::string &calibfolder);

  private:
    bool isLastConfiguration(const char *config, unsigned int length);
    void incrementConfiguration(char *config, unsigned int length);
    bool isWantedTLOutConfiguration(const char *config, unsigned int length);
    bool isWantedRFConfiguration(const char *config, unsigned int length);
    float getMatrixElement(const char *configTLOut, const char *configRF, const float *effReal, const float *effFake, unsigned int length);
    float getInverseFactor(const char *configRF, const char *configTLIn, const float *effReal, const float *effFake, unsigned int length);
    float getEfficiencyVariance(const char *configTLOut, const char *configRF, const char *configTLIn, float matrixElement, float inverseFactor, const float *effReal, const float *effFake, const float *uEffReal, const float *uEffFake, unsigned int length);

  private:
    static float m_etabins[3];
    static float m_ptbins[6];
   
    TFile * m_file;

    TH2F * h_eff_fake_70;
    TH2F * h_eff_fake_stat_70;
    TH2F * h_eff_fake_syst_70;

  };
}
#endif // not MATRIXMETHOD_BJETS_H

